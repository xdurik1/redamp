import requests as reqs
import re
import database as db

def split_adress(url):
    try:
        domain = re.match("(http[s]?:\/\/)?(www.)?([^\/\s]+)+(.*)", url)
        host_name = domain.group(3)
        path = domain.group(4)
        return host_name,path
    except Exception as ex:
        print(ex)

def get_origin(url):
    try:
        domain = re.search("(http[s]?:\/\/)?(www.)?([^\/\s]+)+(.*)", url)
        origin = domain.group(3)
        return origin
    except Exception as ex:
        print(ex)

def input_processing(data, origin):
    try:
        connection = db.connect_db()
        for line in data.text.splitlines():
            line = re.sub(r"#.*", "", line)

            host_name, path = split_adress(line)

            ip = re.match("^\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}$", host_name)
            if ip:
                try:
                    db.add_domain(connection=connection,host_name=host_name, path=path, whole_input=line, origin=origin )
                except Exception as ex:
                    print(ex)
            else:
                try:
                    db.add_ip(connection=connection, host_name=host_name, path=path, whole_input=line, origin=origin)
                except Exception as ex:
                    print(ex)
    except Exception as ex:
        print(ex)

def introduction():
    try:
        while True:
            print("What would you like to do?")
            choice = input("[1] - init DB   [2] - process address ")
            if (choice == "1"):
                connection = db.connect_db()
                print("init")
                db.initDb(connection=connection)
                break
            elif choice == "2":
                break
            else:
                print("Incorrect input, try it again")
        return choice
    except Exception as ex:
        print(ex)

def get_url():
    try:
        url = str(input("Input address "))
        response = reqs.get(url, stream=True)
        return url, response
    except Exception as ex:
        print(ex)
        get_url()