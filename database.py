import psycopg2
import settings

def connect_db():
    try:
        connection_string = "dbname=%s user=%s password=%s host=%s port=%s" %(settings.DB_NAME, settings.DB_USER, settings.DB_PASS, settings.DB_HOST, settings.DB_PORT)
        conn = psycopg2.connect(connection_string)
        return conn

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

def add_domain(connection, host_name, path, whole_input, origin):
    try:
        cur = connection.cursor()
        cur.execute(""" INSERT INTO url_domain (host_name, path, whole_input, origin) 
                        VALUES (%s, %s, %s, %s)""",(host_name, path, whole_input, origin))
        connection.commit()
        cur.close()
    except Exception as ex:
        print(ex)

def add_ip(connection, host_name, path, whole_input, origin):
    try:
        cur = connection.cursor()
        cur.execute(""" INSERT INTO ip_domain (host_name, path, whole_input, origin) 
                        VALUES (%s, %s, %s, %s)""",(host_name, path, whole_input, origin))
        connection.commit()
        cur.close()
    except Exception as ex:
        print(ex)

def initDb(connection):
    cur = connection.cursor()

    commands = (
            """
            DROP TABLE IF EXISTS url_domain
            """,
            """
            DROP SEQUENCE IF EXISTS url_domain_id_seq
            """,
            """
            CREATE SEQUENCE url_domain_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;
            """,
            """CREATE TABLE url_domain(
                            url_domain_id integer DEFAULT nextval('url_domain_id_seq') NOT NULL,
                            host_name character varying(100) NOT NULL,
                            path character varying(2000),
                            whole_input character varying(2100) NOT NULL,
                            origin character varying(100) NOT NULL,
                            CONSTRAINT url_domain_idPK PRIMARY KEY (url_domain_id));
            """,
            """
            DROP TABLE IF EXISTS ip_domain
            """,
            """
            DROP SEQUENCE IF EXISTS ip_id_seq
            """,
            """
            CREATE SEQUENCE ip_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1
            """,
            """
            CREATE TABLE ip_domain(
                          ip_id integer DEFAULT nextval('ip_id_seq') NOT NULL,
                          host_name character varying(100) NOT NULL,
                          path character varying(2000),
                          whole_input character varying(2100) NOT NULL,
                          origin character varying(100) NOT NULL,
                          CONSTRAINT ip_idPK PRIMARY KEY (ip_id)
                        )
            """)

    try:
        for command in commands:
            cur.execute(command)
            connection.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if connection is not None:
            cur.close()



