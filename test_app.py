import pytest
from functions import split_adress
from functions import get_origin

pytest.input_ip = "189.151.246.61"
pytest.input_domain = "https://amazon-email.cloud/pc/"
pytest.source = "https://openphish.com/feed.txt"


def test_split_adress_ip():
    host_name, path = split_adress(pytest.input_ip)
    assert type(host_name) == str
    assert host_name == "189.151.246.61"
    assert path == ""

def test_split_adress_domain():
    host_name, path = split_adress(pytest.input_domain)
    assert type(host_name) == str
    assert host_name == "amazon-email.cloud"
    assert path == "/pc/"

def test_get_origin():
    origin = get_origin(pytest.source)
    assert type(origin) == str
    assert origin == "openphish.com"


